import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UsersPage } from '../users/users';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  lists:any[] = [];

  constructor(public navCtrl: NavController) {
    this.lists.push({
      name: 'Ivonne'
    }); 

    this.lists.push({
      name: 'Carolina'
    }); 

    this.lists.push({
      name: 'Barco'
    });  
  }

  goToUsersPage(){
    this.navCtrl.push(UsersPage);
  }

}
